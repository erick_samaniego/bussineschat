package com.nopalgames.bussinesschat;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterContactos extends BaseAdapter {

	protected Activity activity;
	protected ArrayList<Contacto> contactos;
	public AdapterContactos(Activity activity, ArrayList<Contacto> contactos) {
		this.activity = activity;
		this.contactos = contactos;
		// TODO Auto-generated constructor stub
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return contactos.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return contactos.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return contactos.get(arg0).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		viewHolder viewholder;
		Contacto contacto = contactos.get(position);

		if(convertView == null){
			LayoutInflater inf = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);	
			v = inf.inflate(R.layout.contacto_item, null);
			viewholder = new viewHolder();
			viewholder.nombre = (TextView)v.findViewById(R.id.Usuario);
			viewholder.estado = (TextView)v.findViewById(R.id.Estado);
			viewholder.img = (ImageView)v.findViewById(R.id.Photo);
			v.setTag(viewholder);
			
		}
		
		viewholder = (viewHolder)v.getTag();
		viewholder.nombre.setText(contacto.getName());
		viewholder.img.setImageDrawable(contacto.getPhoto());
		viewholder.estado.setText(contacto.getEstado());
		
		return v;
	}
	static class viewHolder{
		TextView nombre;
		ImageView img;
		TextView estado;
		
	}
}



