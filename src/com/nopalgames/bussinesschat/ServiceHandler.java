package com.nopalgames.bussinesschat;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class ServiceHandler {

	static String response = null;
	public static final int GET = 1;
	public static final int POST = 2;
	
	public ServiceHandler() {
		// TODO Auto-generated constructor stub
	}
	public String makeServiceCall(String url, int method){
		return this.makeServiceCall(url, method,null);
	}
	
	public String makeServiceCall(String url, int method, List<NameValuePair> params){
		
		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpEntity httpentity = null;
			HttpResponse httpresponse = null;
			
			if(method==POST){
				HttpPost httppost = new HttpPost(url);
				if(params!=null){
					httppost.setEntity(new UrlEncodedFormEntity(params));
					
				}
				httpresponse = httpClient.execute(httppost);
			}else if(method == GET){
				if(params!=null){
					String paramString = URLEncodedUtils.format(params, "utf-8");
					url += "?" + paramString;
				}
				android.util.Log.v("send",url);
				HttpGet httpget = new HttpGet(url);
				httpresponse = httpClient.execute(httpget);
			}
			httpentity = httpresponse.getEntity();
			response = EntityUtils.toString(httpentity);
			
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
		
	}
	
}
