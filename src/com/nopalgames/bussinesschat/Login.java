package com.nopalgames.bussinesschat;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
public class Login extends Activity {
	public String user;
	public String pass;
    private static String url = "http://www.nopalgames.com/GCM/contactos.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Button Accept = (Button)findViewById(R.id.Login);
        final EditText Usuario = (EditText)findViewById(R.id.Usuario);
        final EditText Pass = (EditText)findViewById(R.id.Pass);
      
        Accept.setOnClickListener(new View.OnClickListener(){
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
		        user = Usuario.getText().toString();
		        pass = Pass.getText().toString();
				new LoginCheck().execute();

			}
		});
        
        
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
	JSONArray contactsJSON = null;
    public class LoginCheck extends AsyncTask<Void,Void,Void>{
    	public int seArmo = 0;
		@Override
		protected Void doInBackground(Void... arg0) {
			
			ServiceHandler sh = new ServiceHandler();
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);  
			nameValuePairs.add(new BasicNameValuePair("user", user));  
			nameValuePairs.add(new BasicNameValuePair("pass", pass));  
			nameValuePairs.add(new BasicNameValuePair("accion", "Login"));  

			String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST,nameValuePairs);
			if(jsonStr!=null){
				try {
					
					JSONObject jsonObj = new JSONObject(jsonStr);
					contactsJSON = jsonObj.getJSONArray("contactos");
					android.util.Log.v("dd",jsonStr);


					if(contactsJSON.length()>0){
						seArmo = 1;
						List<NameValuePair> nameValuePairs2 = new ArrayList<NameValuePair>(3); 
						JSONObject contacto = contactsJSON.getJSONObject(0);
						String id = contacto.getString("id");
						nameValuePairs2.add(new BasicNameValuePair("id",(id)));  
						nameValuePairs2.add(new BasicNameValuePair("Estado", "1"));  
						nameValuePairs2.add(new BasicNameValuePair("accion", "Estado"));  
						String jsonStr2 = sh.makeServiceCall(url, ServiceHandler.POST,nameValuePairs2);


					}else{
						seArmo = 0;
	
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}else{
				seArmo = 0;
			}
			return null;
		}
		public void onPostExecute(Void result){
			super.onPostExecute(result);
			if(seArmo==1){
				JSONObject contacto;
				String id = "";
				try {
					contacto = contactsJSON.getJSONObject(0);
					id = contacto.getString("id");

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				/*Inicia La actividad contactos
				 * 
				 * Enviando el id del usuario para comprobarlo en la otra seccion
				 * 
				 * */
				Intent intent = new Intent(getApplicationContext(), ContactoActivity.class);
				String value = String.valueOf(id);
				intent.putExtra("ID", value);
				startActivity(intent);			
			}else{
				Toast mgeE = Toast.makeText(getApplicationContext(),"Error al iniciar sesion", Toast.LENGTH_SHORT);
				mgeE.show();				
			}
			
			
		}	
    }
}
