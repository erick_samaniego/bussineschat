package com.nopalgames.bussinesschat;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;


public class ContactoActivity extends Activity {
    public final static String EXTRA_MESSAGE = "com.nopalgames.bussineschat.MESSAGE";
    private static String url = "http://www.nopalgames.com/GCM/contactos.php";
	ListView lista;
	Activity activity;
	ArrayList<Contacto> arrayContacto = new ArrayList<Contacto>();
	int idUsuario = 0;
 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	    Intent intent = getIntent();
	    idUsuario = Integer.valueOf(intent.getExtras().getString("ID"));
	    Log.d("ID",String.valueOf(idUsuario));
		setContentView(R.layout.activity_contactos);
		lista = (ListView)findViewById(R.id.ListaContacto);
		activity = this;
		
		new getContacts().execute();
	}
	JSONArray contactsJSON = null;
	private class getContacts extends AsyncTask<Void, Void, Void>{
		
		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			ServiceHandler sh = new ServiceHandler();
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);  
			nameValuePairs.add(new BasicNameValuePair("id", String.valueOf(idUsuario)));  
			nameValuePairs.add(new BasicNameValuePair("accion", "LoadContactos"));  
			String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST,nameValuePairs);
			if(jsonStr!=null){
				try {
					JSONObject jsonObj = new JSONObject(jsonStr);
					contactsJSON = jsonObj.getJSONArray("contactos");

					for(int i = 0; i<contactsJSON.length(); i++){
						JSONObject contacto = contactsJSON.getJSONObject(i);
						
						String usuario = contacto.getString("nombre");
						int Estado = Integer.valueOf(contacto.getString("estado"));
						int ID = Integer.valueOf(contacto.getString("id"));
						URL url = new URL(contacto.getString("img"));
						String imgStr = contacto.getString("url");

						Drawable img = Drawable.createFromStream(url.openStream(), imgStr);
						
						Log.d("dd",usuario);
						Contacto contacto1;
						String EstadoStr = (Estado==1)?"Online":"Offline";
						contacto1 = new Contacto(img, usuario, EstadoStr,ID);
						arrayContacto.add(contacto1);
						

					}

					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return null;
		}
		@Override
		public void onPostExecute(Void result){
			super.onPostExecute(result);
			AdapterContactos adapter = new AdapterContactos(ContactoActivity.this, arrayContacto);
			lista.setAdapter(adapter);
			lista.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View arg1, int position,
						long id) {
					Toast mge = Toast.makeText(getApplicationContext(),String.valueOf(id), Toast.LENGTH_SHORT);
					mge.show();
					Intent intent = new Intent(getApplicationContext(),ChatActivity.class);
					String value = String.valueOf(id);
					Bundle extras = new Bundle();
					extras.putString("IDTuyo",value);
					extras.putString("IDMio",String.valueOf(idUsuario));
					intent.putExtras(extras);
					startActivity(intent);
					
				}
			});				
			
		}
		
	}
	
}
