package com.nopalgames.bussinesschat;

import android.graphics.drawable.Drawable;

public class Contacto {
	protected Drawable Photo; 
	protected String Name; 
	protected String Estado;
	protected long id;
	
	public Contacto(Drawable photo, String name, String estado){
		super();
		this.Photo  = photo;
		this.Name   = name;
		this.Estado = estado;
	}
	public Contacto(Drawable photo, String name, String estado, long id){
		super();
		this.Photo  = photo;
		this.Name   = name;
		this.Estado = estado;
		this.id 	= id;
	}
	public Drawable getPhoto(){
		return this.Photo;
	}
	public void setPhoto(Drawable photo){
		this.Photo = photo;
	}
	public String getName(){
		return this.Name;
	}
	public void setName(String name){
		this.Name = name;
	}
	public String getEstado(){
		return this.Estado;
	}
	public void setEstado(String estado){
		this.Estado = estado;
	}	
	public long getId(){
		return this.id;
	}
	public void setId(long id){
		this.id = id;
	}

}
