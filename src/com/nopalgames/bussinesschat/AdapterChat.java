package com.nopalgames.bussinesschat;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterChat extends BaseAdapter {

	protected Activity activity;
	protected ArrayList<Chat> chats;
	public AdapterChat(Activity activity, ArrayList<Chat> chats) {
		this.activity = activity;
		this.chats = chats;
		// TODO Auto-generated constructor stub
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return chats.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return chats.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return chats.get(arg0).getID();
	}

	@Override
	public View getView(int position, View converView, ViewGroup parent) {
		viewHolder viewholder;
		View v  = converView;
		Chat chat = chats.get(position);
		if(converView == null ){
			LayoutInflater linf = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if(chat.IO==1){
				v = linf.inflate(R.layout.mensaje_item, null);
			}else{
				v = linf.inflate(R.layout.mensaje_itemodd, null);

			}
			viewholder = new viewHolder();
			viewholder.texto = (TextView)v.findViewById(R.id.text);
			v.setTag(viewholder);
			
		}
		viewholder = (viewHolder)v.getTag();
		viewholder.texto.setText(chat.Mensaje);

		

		return v;
	}
	static class viewHolder{
		TextView texto;
	}

}
