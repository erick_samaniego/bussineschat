package com.nopalgames.bussinesschat;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class ChatActivity  extends Activity{

    private static String url = "http://www.nopalgames.com/GCM/contactos.php";
	ListView lista;
	Activity activity;
	ArrayList<Chat> arrayMensajes = new ArrayList<Chat>();
	int IDMio=0;
	int idUsuario = 0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chat_activity);
		
	    Intent intent = getIntent();
	    Bundle extras = intent.getExtras();
	    
	    String message = intent.getExtras().getString("IDTuyo");
	    TextView texto = (TextView)findViewById(R.id.Mensaje);
	    texto.setText(message);
		
	    ListView lista = (ListView)findViewById(R.id.ListMensajes);




	}
	JSONArray mensajeJSON = null;
	private class getMessages extends AsyncTask<Void, Void, Void>{
		
		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			ServiceHandler sh = new ServiceHandler();
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);  
			
			nameValuePairs.add(new BasicNameValuePair("idMio", String.valueOf(IDMio)));
			nameValuePairs.add(new BasicNameValuePair("idTuyo", String.valueOf(idUsuario)));
			nameValuePairs.add(new BasicNameValuePair("accion", "LoadMessages"));  
			
			String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST,nameValuePairs);
			
			if(jsonStr!=null){
				try {
					JSONObject jsonObj = new JSONObject(jsonStr);
					mensajeJSON = jsonObj.getJSONArray("mensajes");
					
					for(int i = 0; i<mensajeJSON.length(); i++){
						JSONObject contacto = mensajeJSON.getJSONObject(i);
						
						String usuario = contacto.getString("nombre");
						int Estado = Integer.valueOf(contacto.getString("estado"));
						int ID = Integer.valueOf(contacto.getString("id"));
						URL url = new URL(contacto.getString("img"));
						String imgStr = contacto.getString("url");

						Drawable img = Drawable.createFromStream(url.openStream(), imgStr);
						
						
						ArrayList<Chat> arrayChat = new ArrayList<Chat>();
						Chat chat;
						
						Log.d("dd",usuario);
						Chat chat;
						String EstadoStr = (Estado==1)?"Online":"Offline";
						chat = new Chat(img, usuario, EstadoStr,ID);
						
						arrayMensajes.add(contacto1);
						

					}

					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return null;
		}
		@Override
		public void onPostExecute(Void result){
			super.onPostExecute(result);
			AdapterChat adapter = new AdapterChat(ChatActivity.this, arrayMensajes);
			lista.setAdapter(adapter);
			lista.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View arg1, int position,
						long id) {
					Toast mge = Toast.makeText(getApplicationContext(),String.valueOf(id), Toast.LENGTH_SHORT);
					mge.show();
					Intent intent = new Intent(getApplicationContext(),ChatActivity.class);
					String value = String.valueOf(id);
					intent.putExtra("ID", value);
					startActivity(intent);
					
				}
			});				
			
		}
		
	}
	
}
