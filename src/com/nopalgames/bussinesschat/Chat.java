package com.nopalgames.bussinesschat;

public class Chat {
	protected String Mensaje;
	protected String Usuario;
	protected int IO;
	protected String DateTime;
	protected long ID;
	
	public Chat(String usuario, String mensaje, String datetime, int io, long id) {
		this.Usuario = usuario;
		this.Mensaje = mensaje;
		this.DateTime = datetime;
		this.IO = io;
		this.ID = id;	
	
	}
	public void setMensaje(String mensaje) {
		Mensaje = mensaje;
	}
	public String getMensaje() {
		return Mensaje;
	}
	public void setUsuario(String usuario) {
		Usuario = usuario;
	}
	public String getUsuario() {
		return Usuario;
	}
	public void setIO(int iO) {
		IO = iO;
	}
	public int getIO() {
		return IO;
	}
	public void setID(long iD) {
		ID = iD;
	}
	public long getID() {
		return ID;
	}
	public void setDateTime(String dateTime) {
		DateTime = dateTime;
	}
	public String getDateTime() {
		return DateTime;
	}
	

}
